getwd()
library(data.table)
library(plyr)
library(dplyr)
library(ggplot2)
library(imputeTS)
library(RColorBrewer)

###################
#### VEHICLES #####
###################


vehicles_data <- fread("vehicles_hdr.csv", verbose = F, stringsAsFactors = F)
vehicles_data$EURO[vehicles_data$EURO==0] <- NA
vehicles_data$EURO[vehicles_data$EURO==1] <- 0
vehicles_data$EURO[vehicles_data$EURO==2] <- 1
vehicles_data$EURO[vehicles_data$EURO==3] <- 2
vehicles_data$EURO[vehicles_data$EURO==4] <- 3
vehicles_data$EURO[vehicles_data$EURO==5] <- 4
vehicles_data$EURO[vehicles_data$EURO==6] <- 5
vehicles_data$EURO[vehicles_data$EURO==7] <- 6
vehicles_data$VType[vehicles_data$VType==0] <- NA
vehicles_data$FType[vehicles_data$FType==0] <- NA
vehicles_data$DPF[vehicles_data$DPF==0] <- NA
vehicles_data$Length[vehicles_data$Length==0] <- NA

##################
## replacing NA ##
##################

###Random imputation for Vtype based on EURO

Euro_Vtype <- table(vehicles_data$EURO, vehicles_data$VType)
p_Euro_Vtype <- prop.table(Euro_Vtype)
apply(p_Euro_Vtype, MARGIN = 2, sum)
p <- c(0.005, 0.004, 0.073, 0.918)
vect <- c()
for (i in 1:length(p)){vect[i] <- 133368*p[i]}
vect <- round(vect,digits = 0)
vector_1 <- rep(1,667)
vector_2 <- rep(2, 533)
vector_3 <- rep(3,9736)
vector_4 <- rep(4, 122432)
all_vector <- c(vector_1, vector_2, vector_3, vector_4)
set.seed(1)
t <- sample(all_vector, size = length(all_vector), replace = F)
t <- as.integer(t)
vehicles_data[is.na(vehicles_data$VType)]$VType <- t
value <- table(vehicles_data$VType, vehicles_data$EURO)
barplot(as.matrix(value), main = "EURO vs VType", col = brewer.pal(n = 6, name = "Set1"))
legend("topleft", c("1","2","3","4"), cex = 0.8, fill = brewer.pal(n = 6, name = "Set1"))

rm(vect,vector_1,vector_2, vector_3, vector_4, value)



##Random imputation for FType based on EURO

Euro_Ftype <- table(vehicles_data$EURO, vehicles_data$FType)
p_Euro_Ftype <- prop.table(Euro_Ftype)
apply(p_Euro_Ftype, MARGIN = 2, sum)
p_ <- c(0.40, 0.51, 0, 0.08, 0.01)
vect_ <-c()
for(i in 1:length(p_)){vect_[i] <- 133370*p_[i]}
vect_ <- round(vect_,digits = 0)
vector__1 <- rep(1,53347)
vector__2 <- rep(2, 68018)
vector__3 <- rep(3,2)
vector__4 <- rep(4, 10669)
vector__5 <- rep(5,1334)
all_vector1 <- c(vector__1, vector__2, vector__3,vector__4, vector__5)
set.seed(4)
t_ <- sample(all_vector1, size = length(all_vector1), replace = F)
t_ <- as.integer(t_)
vehicles_data[is.na(vehicles_data$FType)]$FType <- t_
value <- table(vehicles_data$FType, vehicles_data$EURO)
barplot(as.matrix(value), main = "EURO vs FType", col = brewer.pal(n = 5, name = "Set1"))
legend("topleft", c("1","2","3","4","5"), cex = 0.8, fill = brewer.pal(n = 5, name = "Set1"))

rm(vector__1, vector__2, vector__3,vector__4,vector__5, t_, value)

###random imputation for DPF based on its previous distibuition

set.seed(4)
t__ <- sample(x = c(1,2), size = 131257, replace = T, prob = c(.33,.66))
t__ <- as.integer(t__)
vehicles_data[is.na(vehicles_data$DPF)]$DPF <- t__
rm(t__)

##random imputation for EURO based on Ftype

no_missing <- vehicles_data[is.na(vehicles_data$EURO) == FALSE]
missing <- vehicles_data[is.na(vehicles_data$EURO) == TRUE]
EURO_based_fType <- table(no_missing$EURO, no_missing$FType)
apply(prop.table(EURO_based_fType),MARGIN = 1,sum)
q <- as.vector(apply(prop.table(EURO_based_fType),MARGIN = 1,sum))
q_<-c()
for(i in 1:length(q)){q_[i] <- 133460*q[i]}
q_ <- round(q_,digits = 0)
r <- c(0,1,2,3,4,5,6)

l = list()
for(i in 1:length(q_)){
  l[[i]] <- rep(r[i], q_[i])
}
q_all <- unlist(l)
set.seed(5)
y <- sample(q_all, size = length(q_all), replace = F)
y_ <- as.double(y)
vehicles_data[is.na(vehicles_data$EURO)]$EURO <- y_
rm(q_, r, y, y_, no_missing,missing)

##random imputation for length based on its known distribution

no_missing_length <- vehicles_data[!is.na(vehicles_data$Length)]$Length
k <- sample(x = no_missing_length, size = 129604, replace = T)
k <- as.integer(k)
vehicles_data[is.na(vehicles_data$Length)]$Length <- k
rm(k, no_missing_length)




###############
####TRANSIT####
###############
completeTransits <- fread("transit_hdr.csv")

###Some quick analysis###

####casting for passages###
transit_one <- completeTransits[!duplicated(completeTransits$Plate),]
transit_more <- completeTransits[duplicated(completeTransits$Plate),]


####converting timestamp to POSIXct; it will take a while###
transit_one$Timestamp <- as.POSIXct(strptime(transit_one$Timestamp, "%Y-%m-%d %H"))
transit_more$Timestamp <- as.POSIXct(strptime(transit_more$Timestamp, "%Y-%m-%d %H"))

##looking at the average distribution for more than one passages
prova <- transit_more%>%group_by(Timestamp)%>%summarize(Plate)
prova <- prova%>%group_by(Timestamp)%>%summarize(n())
prova <- as.data.frame(prova)
prova <- prova[order(prova$Timestamp, decreasing = F),]
t <- aggregate(ts(prova[,2], freq = 24),1,mean)
y <- as.data.frame(matrix(data = c(t, seq(1, length(t))),byrow = F,ncol = 2))


##looking at the average distribution for only one passage
prova_1 <- transit_one%>%group_by(Timestamp)%>%summarize(Plate)
prova_1 <- prova_1%>%group_by(Timestamp)%>%summarize(n()) 
prova_1 <- as.data.frame(prova_1)
prova_1 <- prova_1[order(prova_1$Timestamp,decreasing = F),]
t_ <- aggregate(ts(prova_1[,2], frequency = 24),1,mean)
y_ <- as.data.frame(matrix(data = c(t_, seq(1, length(t_))),byrow = F,ncol = 2))

y <- cbind(y_, y)
y <- subset(y, select = c(1,3,4))
colnames(y) <- c("more passages","one passage","day")
my_table <- melt(y, id.vars = "day")
head(my_table)
tiff('test.tiff', units="in", width=5, height=4, res=300, compression = 'lzw')
ggplot(data = my_table,aes(day, value)) + geom_point(aes(color = variable)) + geom_line(aes(color = variable)) + scale_color_brewer(palette = "Set1") + 
  theme_minimal() + ggtitle("daily transit average") + 
  theme(legend.title = element_blank(), axis.title = element_blank())
dev.off()
###why more passages look to behave in the opposite way than the only one in a both daily and whole period trend?

total_vehicles <- merge(vehicles_data, completeTransits, by.x = "Plate", by.y = "Plate")

total_vehicles$categorical <- ifelse(total_vehicles$Length<2000, 0, ifelse(total_vehicles$Length>=2000 & total_vehicles$Length<4000, 1, ifelse(total_vehicles$Length>=4000 & total_vehicles$Length<6000,2,3) ))

target <- total_vehicles[!duplicated(total_vehicles$Plate),]
value <- as.data.frame(table(target$categorical))
value$Var1 <- factor(value$Var1)
levels(value$Var1) <- c("0","1","2","3")
tiff('test2.tiff', units="in", width=5, height=4, res=300, compression = 'lzw')
ggplot(value, aes(x = Var1, y = (Freq)/nrow(target))) + geom_bar(stat = "identity") + theme(axis.title = element_blank()) + ggtitle("Classes of car length")
dev.off()
##looking at different DPF car transit
no_dpf <- total_vehicles%>%filter(DPF == 2)
yes_dpf <- total_vehicles%>%filter(DPF == 1)
no_dpf$Length <- NULL
yes_dpf$Length <- NULL

no_dpf$Timestamp <- as.POSIXct(strptime(no_dpf$Timestamp, "%Y-%m-%d %H"))
no_dpf <- no_dpf[order(no_dpf$Timestamp,decreasing = F),]
no_dpf <- as.data.frame(no_dpf)
no_dpf <- no_dpf%>%group_by(Timestamp)%>%summarize(n())
no_dpf <- aggregate(ts(no_dpf$`n()`,frequency = 24),1,mean)

yes_dpf$Timestamp <- as.POSIXct(strptime(yes_dpf$Timestamp, "%Y-%m-%d %H"))
yes_dpf <- yes_dpf[order(yes_dpf$Timestamp,decreasing = F),]
yes_dpf <- as.data.frame(yes_dpf)
yes_dpf <- yes_dpf%>%group_by(Timestamp)%>%summarize(n())
yes_dpf <- aggregate(ts(yes_dpf$`n()`,frequency = 24),1,mean)

g <- as.data.frame(matrix(data = c(yes_dpf, no_dpf, seq(1, length(yes_dpf))),byrow = F,ncol = 3))
colnames(g) <- c("Yes_DPF", "No_DPF", "Day")
g <- melt(g, id.vars = "Day")
tiff('test1.tiff', units="in", width=5, height=4, res=300, compression = 'lzw')
ggplot(data =g, aes(Day, value)) + geom_line(aes(color = variable)) + geom_point(aes(color = variable)) + 
  ggtitle("DPF transit trend") + theme_minimal() + 
  theme(legend.title = element_blank(), axis.title = element_blank()) 
dev.off()

###looking at the whole number of passages by hour
count_one <- transit_one%>%select(Timestamp, Plate)%>%group_by(Timestamp)%>%summarize(n())
count_more <- transit_more%>%select(Timestamp, Plate)%>%group_by(Timestamp)%>%summarize(n())
count_one <- as.data.frame(count_one)
count_more <- as.data.frame(count_more)
count_one <- count_one[order(count_one$Timestamp, decreasing = F),]
count_more <- count_more[order(count_more$Timestamp, decreasing = F),]
collective <- cbind(count_more, count_one[2])
collective <- cbind(collective[1], apply(collective[2:3], 1, sum))
colnames(collective) <- c("Date", "Total transit")

###################################################
####### whole transit features table ##############
###################################################

total_vehicles$EURO <- factor (total_vehicles$EURO, levels=0:6, labels=c("0","1","2","3","4","5","6"))
total_vehicles$VType <- factor (total_vehicles$VType, levels=1:4, labels=c("Other","Bus","Freight","People"))
total_vehicles$FType <- factor (total_vehicles$FType, levels=1:5, labels=c("Petrol", "Diesel","Electric","LPG_Gas","Hybrid"))
total_vehicles$DPF <- factor (total_vehicles$DPF, levels=1:2, labels=c("Yes","No"))
total_vehicles$Timestamp <- as.POSIXct(strptime(total_vehicles$Timestamp, "%Y-%m-%d %H"))
names(Whole_features)[1] <- "DateTime"

attach(total_vehicles)
Euro_time <- table(EURO, DateTime)
Euro_time <- as.data.frame(Euro_time)
Euro_time_cast <- dcast(Euro_time, formula = DateTime ~ EURO, value.var = "Freq")

Vtype_time <- table(VType, DateTime)
Vtype_time <- as.data.frame(Vtype_time)
Vtype_time_cast <- dcast(Vtype_time, formula = DateTime ~ VType, value.var = "Freq")

Ftype_time <- table(FType, DateTime)
Ftype_time <- as.data.frame(Ftype_time)
Ftype_time_cast <- dcast(Ftype_time, formula = DateTime ~ FType, value.var = "Freq")

DPF_time <- table(DPF, DateTime)
DPF_time <- as.data.frame(DPF_time)
DPF_time_cast <- dcast(DPF_time, formula = DateTime ~ DPF, value.var = "Freq")

detach(total_vehicles)
Whole_features <- cbind(Euro_time, Vtype_time[,-1], Ftype_time[,-1], DPF_time[,-1])

#write.csv(x = Whole_features, file = "TransitCounts.csv", row.names = F)



