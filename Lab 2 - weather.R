library(data.table)
library(imputeTS)
library(plyr)
library(dplyr)
total_meteo <- read.csv("totalmeteoMF.csv")

names(total_meteo)[6] <- "Description"

#columns: Description, DateTime, Value
myaverage <- function(data, d){
  averages<-ddply(data[data$Description==d ,],c("Description","DateTime"),function(dff) mean(dff$Value))
  averages$Description <- NULL
  colnames(averages) <- c("DateTime",d)
  return (averages)}


total_meteo <- total_meteo%>%select(Description,DateTime,Value)

All=data.frame(DateTime=sort(unique(total_meteo$DateTime)))
values=unique(total_meteo$Description)
for ( v in values) {
  All=merge(All,myaverage(total_meteo, v),all.x=TRUE, by="DateTime")
}

weather_table <- read.csv("weather_station_average.csv")


my_list <- list()
weather_table_ <- as.matrix(weather_table[,-1])

for(i in 1:8){
  my_list[[i]] <- aggregate(ts(weather_table_[,i], frequency = 24),1,mean)
}

mean_temperature <- my_list[[1]]
mean_humidity <- my_list[[2]]
mean_precipitation <- my_list[[3]]
mean_g_radiation <- my_list[[4]]
mean_w_direction <- my_list[[5]]
mean_w_speed <- my_list[[6]]
mean_net_rad <- my_list[[7]]
mean_atm_pressure <- my_list[[8]]


#########
### NA ##
#########

##mean
weather_table$Global.Radiation[is.na(weather_table$Global.Radiation)] <- mean(weather_table$Global.Radiation,na.rm = T)

##mean
weather_table$Wind.Direction[is.na(weather_table$Wind.Direction)] <- mean(weather_table$Wind.Direction ,na.rm = T)

##mean
weather_table$Relative.Humidity[is.na(weather_table$Relative.Humidity)] <- mean(weather_table$Wind.Direction ,na.rm = T)

##mean
weather_table$Wind.Speed[is.na(weather_table$Wind.Speed)] <- mean(weather_table$Wind.Speed ,na.rm = T)

##spline
weather_table$Atmospheric.Pressure <- na.interpolation(weather_table$Atmospheric.Pressure, option = "spline")

##spline
weather_table$Net.Radiation <- na.interpolation(weather_table$Net.Radiation, option = "spline")

#write.csv(x = weather_table, file = "aggregate_weather.csv", row.names = FALSE)



